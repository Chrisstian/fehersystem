<?php

namespace Model;

/**
 * Class User
 */
class User {

    private int     $id;
    private string  $name;
    private string  $email;
    private string  $password;
    private int     $familyStatusId;
    private ?string $birthDate;
    private ?string $webPage;

    public function __construct(string $name,
                                string $email,
                                string $password,
                                int    $familyStatusId,
                                string $birthDate = '',
                                string $webPage = '') {

        $this->name = $name;
        $this->email = $email;
        $this->password = hash('sha256', $password);
        $this->familyStatusId = $familyStatusId;
        $this->birthDate = ($birthDate === '') ? NULL : $birthDate;
        $this->webPage = ($webPage === '') ? NULL : $webPage;
    }

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function setName(string $name): void {
        $this->name = $name;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function setEmail(string $email): void {
        $this->email = $email;
    }

    public function getPassword(): string {
        return $this->password;
    }

    public function setPassword(string $password): void {
        $this->password = hash('sha256', $password);
    }

    public function getFamilyStatusId(): int {
        return $this->familyStatusId;
    }

    public function setFamilyStatusId(int $familyStatusId): void {
        $this->familyStatusId = $familyStatusId;
    }

    public function getBirthDate(): ?string {
        return $this->birthDate;
    }

    public function setBirthDate(string $birthDate): void {
        $this->birthDate = ($birthDate === '') ? NULL : $birthDate;;
    }

    public function getWebPage(): ?string {
        return $this->webPage;
    }

    public function setWebPage(string $webPage): void {
        $this->webPage = ($webPage === '') ? NULL : $webPage;;
    }
}