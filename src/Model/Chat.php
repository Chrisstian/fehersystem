<?php

namespace Model;

/**
 * Class Chat
 */
class Chat {

    private int $id;
    private int $userId;
    private string $message;
    private string $createdAt;

    public function __construct(int    $userId,
                                string $message,
                                string $createdAt) {

        $this->userId = $userId;
        $this->message = $message;
        $this->createdAt = $createdAt;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getUserId(): int {
        return $this->userId;
    }

    public function setUserId(int $userId): void {
        $this->userId = $userId;
    }

    public function getMessage(): string {
        return $this->message;
    }

    public function setMessage(string $message): void {
        $this->message = $message;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }


}