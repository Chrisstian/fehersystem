<?php

namespace Lib;

use Repository\UserRepository;

/**
 * Interface RegistrationInterface
 */
interface UserInterface {

    /**
     * @param object $request
     * @param UserRepository $userRepository
     * @return array
     */
    public function userValidation(object         $request,
                                   UserRepository $userRepository): array;

}