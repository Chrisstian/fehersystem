<?php

namespace Lib;

use PDO;
use PDOException;

/**
 * Class Database
 */
class Database {

    private string $host = 'localhost';
    private string $dbName = 'feher_system';
    private string $username = 'admin';
    private string $password = 'admin1234';
    protected PDO  $conn;

    /**
     * Adatbázis kapcsolat felépítése
     *
     * @return PDO
     */
    public function getConnection(): PDO {

        try {
            $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->dbName, $this->username, $this->password);
            $this->conn->exec('set names utf8');
        } catch(PDOException $exception) {
            echo 'Adatbázis kapcsolódási hiba: ' . $exception->getMessage();
        }

        return $this->conn;
    }
}