<?php

namespace Lib;

/**
 * Class Utils
 */
class Utils {

    /**
     * E-mail formátum validálás
     *
     * @param string $email
     * @return bool
     */
    public static function emailValidation(string $email): bool {
        if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Jelszó hashelés
     *
     * @param string $password
     * @return string
     */
    public static function setupHashPassword(string $password): string {
        return hash('sha256', $password);
    }

    /**
     * Az üres string-nek NULL értéket ad
     *
     * @param string $parameter
     * @return string|null
     */
    public static function setupStringDefaultNull(string $parameter): ?string {
        return ($parameter === '') ? NULL : $parameter;
    }

    /**
     * Jelszó validálás
     *
     * @param string $password
     * @return bool
     */
    public static function passwordValidation(string $password): bool {
        $regex = '/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/';

        if((strlen($password) < 4) || preg_match($regex, $password) === 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Életkor kiszámítása
     *
     * @param string $date
     * @return int
     */
    public static function calculateAge(string $date): int {
        $now = date('Y-m-d');
        $diff = abs(strtotime($now) - strtotime($date));
        return (int)floor($diff / (365 * 60 * 60 * 24));
    }
}