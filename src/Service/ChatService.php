<?php

namespace Service;

use Model\Chat;
use Repository\ChatRepository;

require_once('../Model/Chat.php');
require_once('../Repository/ChatRepository.php');

/**
 * Class ChatService
 */
class ChatService {

    private ChatRepository $chatRepository;

    public function __construct() {
        $this->chatRepository = new ChatRepository;
    }

    /**
     * Chat osztály rögzítése
     *
     * @param object $request
     * @return void
     */
    public function createMessage(object $request): void {
        $createdAt = date('Y-m-d H:i:s');
        $message = new Chat($request->user_id, $request->message, $createdAt);
        $this->chatRepository->createMessage($message);
    }

    /**
     * Összes üzenet lekérdezése
     *
     * @param int $lastMessageId
     * @return array
     */
    public function listMessages(int $lastMessageId): array {
        return $this->chatRepository->listMessages($lastMessageId);
    }

    /**
     * Utolsó üzenet ID lekérdezése
     *
     * @return int
     */
    public function showLastMessageId(): int {
        $lastMessageId = $this->chatRepository->searchLastMessageId();
        return ($lastMessageId === false) ? 0 : $lastMessageId;
    }
}