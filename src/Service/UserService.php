<?php

namespace Service;

session_start();

use Lib\Utils;
use Model\User;
use Repository\UserRepository;

require_once('../Lib/Utils.php');
require_once('../Model/User.php');
require_once('../Repository/UserRepository.php');


/**
 * Class UserService
 */
class UserService {

    private UserRepository $userRepository;

    public function __construct() {
        $this->userRepository = new UserRepository;
    }

    /**
     * User osztály rögzítése
     *
     * @param object $request
     * @return void
     */
    public function createUser(object $request): void {

        $user = new User($request->name, $request->email, $request->password, $request->family_type, $request->birth_date, $request->web_page);
        $this->userRepository->createUser($user);
    }

    /**
     * Bejelentkezés
     *
     * @param object $request
     * @return bool
     */
    public function loginUser(object $request): bool {

        $result = false;
        $request->password = Utils::setupHashPassword($request->password);
        $login = $this->userRepository->login($request);

        if($login !== false) {
            $_SESSION['user'] = $login;
            $result = true;
        }

        return $result;
    }

    /**
     * Felhasználó adatainak lekérdezése
     *
     * @param int $userId
     * @return mixed
     */
    public function showUserById(int $userId): mixed {

        $userData = $this->userRepository->searchUserDataById($userId);
        if($userData !== false) {
            if($userData['birth_date'] !== NULL) {
                $userData['age'] = Utils::calculateAge($userData['birth_date']);
            }
            unset($userData['password']);
        }

        return $userData;
    }

    /**
     * Felhasználó módosítása
     *
     * @param object $request
     * @return array
     */
    public function modifyUser(object $request): array {

        $data = array();

        if($request->password !== '') {
            $request->password = Utils::setupHashPassword($request->password);
            $this->userRepository->modifyPassword($request->user_id, $request->password);
        }

        $request->birth_date = Utils::setupStringDefaultNull($request->birth_date);
        $request->web_page = Utils::setupStringDefaultNull($request->web_page);
        $this->userRepository->modifyUser($request);
        $data['age'] = Utils::calculateAge($request->birth_date);

        return $data;
    }
}