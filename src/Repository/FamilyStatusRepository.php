<?php

namespace Repository;

use PDO;
use Lib\Database;

require_once('../Lib/Database.php');

/**
 * Class FamilyStatusRepository
 */
class FamilyStatusRepository {

    private object $conn;

    public function __construct() {
        $this->conn = (new Database())->getConnection();
    }

    /**
     * Családi státuszok lekérdezése
     *
     * @return array
     */
    public function listFamilyStatuses(): array {

        $query = "SELECT id, name
                  FROM family_status
                  WHERE is_active = :is_active";

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(':is_active', 1, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}