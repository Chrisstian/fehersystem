<?php

namespace Repository;

use PDO;
use Lib\Database;

require_once('../Lib/Database.php');

/**
 * Class UserRepository
 */
class UserRepository {

    private object $conn;

    public function __construct() {
        $this->conn = (new Database())->getConnection();
    }

    /**
     * Bejelentkezés
     *
     * @param object $data
     * @return mixed
     */
    public function login(object $data): mixed {

        $query = "SELECT *
                  FROM user
                  WHERE email = :email
                  AND password = :password
                  AND is_active = :is_active";

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(':email', $data->email, PDO::PARAM_STR);
        $stmt->bindValue(':password', $data->password, PDO::PARAM_STR);
        $stmt->bindValue(':is_active', 1, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Felhasználó adatok lekérdezése ID alapján
     *
     * @param int $id
     * @return mixed
     */
    public function searchUserDataById(int $id): mixed {

        $query = "SELECT id, name, email, password, family_status_id, birth_date, web_page
                  FROM user
                  WHERE id = :id
                  AND is_active = :is_active";

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->bindValue(':is_active', 1, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Új felhasználó rögzítése
     *
     * @param object $user
     * @return void
     */
    public function createUser(object $user): void {

        $query = "INSERT INTO user
                  SET name = :name,
                      email = :email,
                      password = :password,
                      family_status_id = :family_status_id,
                      birth_date = :birth_date,
                      web_page = :web_page,
                      is_active = :is_active,
                      created_at = NOW()";

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(':name', $user->getName(), PDO::PARAM_STR);
        $stmt->bindValue(':email', $user->getEmail(), PDO::PARAM_STR);
        $stmt->bindValue(':password', $user->getPassword(), PDO::PARAM_STR);
        $stmt->bindValue(':family_status_id', $user->getFamilyStatusId(), PDO::PARAM_INT);
        $stmt->bindValue(':birth_date', $user->getBirthDate());
        $stmt->bindValue(':web_page', $user->getWebPage());
        $stmt->bindValue(':is_active', 1, PDO::PARAM_INT);
        $stmt->execute();
    }

    /**
     * Jelszó módosítása
     *
     * @param int $userId
     * @param string $password
     * @return void
     */
    public function modifyPassword(int    $userId,
                                   string $password): void {

        $query = "UPDATE user
                  SET password = :password,
                      modified_at = NOW()
                  WHERE id = :id";

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(':password', $password, PDO::PARAM_STR);
        $stmt->bindValue(':id', $userId, PDO::PARAM_INT);
        $stmt->execute();
    }

    /**
     * Felhasználó adatainak módosítása
     *
     * @param object $data
     * @return void
     */
    public function modifyUser(object $data): void {

        $query = "UPDATE user
                  SET name = :name,
                      email = :email,
                      family_status_id = :family_status_id,
                      birth_date = :birth_date,
                      web_page = :web_page,
                      modified_at = NOW()
                  WHERE id = :id";

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(':name', $data->name, PDO::PARAM_STR);
        $stmt->bindValue(':email', $data->email, PDO::PARAM_STR);
        $stmt->bindValue(':family_status_id', $data->family_type, PDO::PARAM_INT);
        $stmt->bindValue(':birth_date', $data->birth_date, PDO::PARAM_STR);
        $stmt->bindValue(':web_page', $data->web_page, PDO::PARAM_STR);
        $stmt->bindValue(':id', $data->user_id, PDO::PARAM_INT);
        $stmt->execute();
    }

    /**
     * E-mail cím létezésének lekérdezése
     *
     * @param string $email
     * @return mixed
     */
    public function checkEmail(string $email): mixed {

        $query = "SELECT 1 AS found
                  FROM user
                  WHERE email = :email
                  AND is_active = :is_active";

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt->bindValue(':is_active', 1, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_COLUMN);
    }

    /**
     * E-mail cím létezésének lekérdezése (felhasználó adatainak módosításakor)
     *
     * @param int $id
     * @param string $email
     * @return mixed
     */
    public function checkDuplicatedEmail(int    $id,
                                         string $email): mixed {

        $query = "SELECT 1 AS found
                  FROM user
                  WHERE email = :email
                  AND is_active = :is_active
                  AND id != :id";

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt->bindValue(':is_active', 1, PDO::PARAM_INT);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_COLUMN);
    }
}