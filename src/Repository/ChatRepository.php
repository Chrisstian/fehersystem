<?php

namespace Repository;

use PDO;
use Lib\Database;

require_once('../Lib/Database.php');

/**
 * Class ChatRepository
 */
class ChatRepository {

    private object $conn;

    public function __construct() {
        $this->conn = (new Database())->getConnection();
    }

    /**
     * Új üzenet rögzítése
     *
     * @param object $message
     * @return void
     */
    public function createMessage(object $message): void {

        $query = "INSERT INTO chat
                  SET user_id = :user_id,
                      message = :message,
                      created_at = :created_at";

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(':user_id', $message->getUserId(), PDO::PARAM_INT);
        $stmt->bindValue(':message', $message->getMessage(), PDO::PARAM_STR);
        $stmt->bindValue(':created_at', $message->getCreatedAt(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * Üzenetek lekérdezése
     *
     * @param int $messageId
     * @return array
     */
    public function listMessages(int $messageId): array {

        $query = "SELECT c.*, u.name
                  FROM chat AS c
                  INNER JOIN user AS u ON (c.user_id = u.id)
                  WHERE c.id > :message_id
                  ORDER BY c.id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue(':message_id', $messageId, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Utolsó üzenet ID lekérdezése
     *
     * @return mixed
     */
    public function searchLastMessageId(): mixed {

        $query = "SELECT id
                  FROM chat
                  ORDER BY id DESC
                  LIMIT 1";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_COLUMN);
    }
}