<?php

namespace Controller;

use Exception;
use Repository\FamilyStatusRepository;

require_once('../Repository/FamilyStatusRepository.php');

/**
 * Class FamilyStatusController
 */
class FamilyStatusController {

    /**
     * Családi állapotok lekérdezése
     *
     * @return false|string
     * @throws Exception
     */
    public function showFamilyStatuses(): false|string {

        try {
            $familyStatuses = (new FamilyStatusRepository())->listFamilyStatuses();
            $payload = array('status' => 200, 'data' => $familyStatuses, 'message' => null);
            return json_encode($payload);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}