<?php

namespace Controller;

use Exception;
use Service\ChatService;

require_once('../Service/ChatService.php');

/**
 * Class ChatController
 */
class ChatController {

    public ChatService $chatService;

    public function __construct() {
        $this->chatService = new ChatService;
    }

    /**
     * Új üzenet küldése
     *
     * @param object $request
     * @return false|string
     * @throws Exception
     */
    public function sendMessage(object $request): false|string {

        try {
            $status = 401;
            $data = array();

            if((int)$request->user_id === $_SESSION['user']['id']) {
                if(strlen($request->message) === 0) {
                    $status = 422;
                    $data = array('Legalább egy karaktert adjon meg üzenetként!');
                } else {
                    $this->chatService->createMessage($request);
                    $status = 201;
                }
            }

            $payload = array('status' => $status, 'data' => $data, 'message' => null);
            return json_encode($payload);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Üzenetek lekérdezése
     *
     * @param int $lastMessageId
     * @return false|string
     * @throws Exception
     */
    public function showMessages(int $lastMessageId): false|string {

        try {
            $messages = $this->chatService->listMessages($lastMessageId);
            $payload = array('status' => 200, 'data' => $messages, 'message' => null);
            return json_encode($payload);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Utolsó üzenet ID lekérdezése
     *
     * @return false|string
     * @throws Exception
     */
    public function showLastMessageId(): false|string {

        try {
            $lastMessageId = $this->chatService->showLastMessageId();
            $data = array('lastMessageId' => $lastMessageId);
            $payload = array('status' => 200, 'data' => $data, 'message' => null);
            return json_encode($payload);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}