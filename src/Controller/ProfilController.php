<?php

namespace Controller;

use Exception;
use Lib\Utils;
use Lib\UserInterface;
use Repository\UserRepository;
use Service\UserService;

require_once('../Lib/Utils.php');
require_once('../Lib/UserInterface.php');
require_once('../Service/UserService.php');

/**
 * Class ProfilController
 */
class ProfilController implements UserInterface {

    public UserService $userService;

    public function __construct() {
        $this->userService = new UserService;
    }

    /**
     * Felhasználó adatainak lekérdezése
     *
     * @param int $userId
     * @return false|string
     * @throws Exception
     */
    public function showProfilData(int $userId): false|string {

        try {
            $status = 401;
            $data = array();

            if($userId === $_SESSION['user']['id']) {
                $userData = $this->userService->showUserById($userId);
                if($userData !== false) {
                    $status = 200;
                    $data = $userData;
                }
            }

            $payload = array('status' => $status, 'data' => $data, 'message' => null);
            return json_encode($payload);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Felhasználó adatmódosítása
     *
     * @param object $request
     * @return false|string
     * @throws Exception
     */
    public function modifyProfil(object $request): false|string {

        try {
            $data = $this->userService->modifyUser($request);
            $payload = array('status' => 200, 'data' => $data, 'message' => null);
            return json_encode($payload);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Felhasználó adatmódosítás validálása
     *
     * @param object $request
     * @param UserRepository $userRepository
     * @return array
     */
    public function userValidation(object         $request,
                                   UserRepository $userRepository): array {

        $message = array();

        if($request->name === '' || $request->email === '') {
            $message[] = 'A *-al jelölt mezők kitöltése kötelező!';
        }
        if(strlen($request->name) < 3) {
            $message[] = 'Nem megfelelő nevet adott meg!<br/>A névnek minimum 4 karakterből kell állnia.';
        }
        if(Utils::emailValidation($request->email) === false) {
            $message[] = 'Érvénytelen e-mail formátumot adott meg!';
        }
        if($userRepository->checkDuplicatedEmail($request->user_id, $request->email) === 1) {
            $message[] = 'Ez az e-mail cím már szerepel az adatbázisban!';
        }
        if($request->password !== '' && (Utils::passwordValidation($request->password)) === false) {
            $message[] = 'Nem megfelelő jelszót adott meg!<br/>A jelszónak 4 karakterből kell állnia.<br/>Minimum 1 betűt és számot kell, hogy tartalmazzon.';
        }
        if($request->password !== $request->password2) {
            $message[] = 'A két jelszó nem ugyanaz!';
        }
        if($request->birth_date !== '' && $request->birth_date > date('Y-m-d')) {
            $message[] = 'Érvénytelen születési dátumot adott meg!';
        }

        return $message;
    }
}