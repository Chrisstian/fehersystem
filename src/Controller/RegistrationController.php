<?php

namespace Controller;

use Exception;
use Lib\UserInterface;
use Lib\Utils;
use Repository\UserRepository;
use Service\UserService;

require_once('../Lib/Utils.php');
require_once('../Lib/UserInterface.php');
require_once('../Service/UserService.php');

/**
 * Class RegistrationController
 */
class RegistrationController implements UserInterface {

    public UserService $userService;

    public function __construct() {
        $this->userService = new UserService;
    }

    /**
     * Regisztráció
     *
     * @param object $request
     * @return false|string
     * @throws Exception
     */
    public function registration(object $request): false|string {

        try {
            $this->userService->createUser($request);

            $payload = array('status' => 201, 'data' => array(), 'message' => null);
            return json_encode($payload);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Felhasználó adatok validálása
     *
     * @param object $request
     * @param UserRepository $userRepository
     * @return array
     */
    public function userValidation(object         $request,
                                   UserRepository $userRepository): array {

        $message = array();

        if($request->name === '' || $request->email === '' || $request->password === '' || $request->password2 === '') {
            $message[] = 'A *-al jelölt mezők kitöltése kötelező!';
        }
        if(strlen($request->name) < 3) {
            $message[] = 'Nem megfelelő nevet adott meg!<br/>A névnek minimum 4 karakterből kell állnia.';
        }
        if(Utils::emailValidation($request->email) === false) {
            $message[] = 'Érvénytelen e-mail formátumot adott meg!';
        }
        if($userRepository->checkEmail($request->email) === 1) {
            $message[] = 'Ez az e-mail cím már szerepel az adatbázisban!';
        }
        if(Utils::passwordValidation($request->password) === false) {
            $message[] = 'Nem megfelelő jelszót adott meg!<br/>A jelszónak 4 karakterből kell állnia.<br/>Minimum 1 betűt és számot kell, hogy tartalmazzon.';
        }
        if($request->password !== $request->password2) {
            $message[] = 'A két jelszó nem ugyanaz!';
        }
        if($request->birth_date !== '' && $request->birth_date > date('Y-m-d')) {
            $message[] = 'Érvénytelen születési dátumot adott meg!';
        }

        return $message;
    }
}

