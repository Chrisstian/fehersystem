<?php

namespace Controller;

use DOMDocument;
use Exception;

/**
 * Class AnalysisController
 */
class AnalysisController
{

    /**
     * Weboldal elemzése
     *
     * @param object $request
     * @return false|string
     * @throws Exception
     */
    public function analyze(object $request): false|string
    {

        try {
            $status = 200;

            if (!filter_var($request->url, FILTER_VALIDATE_URL)) {
                $status = 422;
                $data = array('Érvénytelen url formátumot adott meg!');
            } else {
                $data = $this->crawlPage($request->url);
            }

            $payload = array('status' => $status, 'data' => $data, 'message' => null);
            return json_encode($payload);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Weboldal adatainak összegyűjtése
     *
     * @param string $url
     * @return array
     */
    private function crawlPage(string $url): array
    {

        $i = 0;
        $result = array();

        $dom = new DOMDocument();
        $dom->loadHTMLFile($url);
        $dom->preserveWhiteSpace = false;

        $domString = $dom->textContent;
        $domArray = str_word_count($domString, 1);

        foreach ($domArray as $value) {
            if ((str_contains($value, 'a') || str_contains($value, 'A')) && $i < 64) {
                if (!in_array($value, $result)) {
                    $result[] = $value;
                    $i++;
                }
            }
        }

        return $result;
    }
}