<?php

namespace Controller;

use Exception;
use Lib\Utils;
use Service\UserService;

require_once('../Lib/Utils.php');
require_once('../Service/UserService.php');

/**
 * Class LoginController
 */
class LoginController {

    public UserService $userService;

    public function __construct() {
        $this->userService = new UserService;
    }

    /**
     * Bejelentkezés
     *
     * @param object $request
     * @return false|string
     * @throws Exception
     */
    public function login(object $request): false|string {

        try {
            $status = 200;
            $data = array();

            $validationMessages = $this->validation($request);
            if(!empty($validationMessages)) {
                $status = 422;
                $data = $validationMessages;
            } else {
                $login = $this->userService->loginUser($request);
                if($login === false) {
                    $status = 422;
                    $data = array('Nem megfelelő e-mail cím vagy jelszó!');
                }
            }

            $payload = array('status' => $status, 'data' => $data, 'message' => null);
            return json_encode($payload);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Bejelentkezési adatok validálása
     *
     * @param object $request
     * @return array
     */
    private function validation(object $request): array {
        $message = array();

        if($request->email === '' || $request->password === '') {
            $message[] = 'Kérjük töltse ki mindkét mezőt!';
        }
        if(Utils::emailValidation($request->email) === false) {
            $message[] = 'Érvénytelen e-mail formátumot adott meg!';
        }

        return $message;
    }
}