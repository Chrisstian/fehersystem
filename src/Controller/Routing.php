<?php

use Controller\AnalysisController;
use Controller\ChatController;
use Controller\FamilyStatusController;
use Controller\LoginController;
use Controller\ProfilController;
use Controller\RegistrationController;
use Repository\UserRepository;

require_once('ChatController.php');
require_once('LoginController.php');
require_once('ProfilController.php');
require_once('AnalysisController.php');
require_once('FamilyStatusController.php');
require_once('RegistrationController.php');
require_once('../Repository/UserRepository.php');

if(!empty($_POST)) {

    try {
        $result = '';
        $request = json_decode(file_get_contents('php://input'), false);

        switch($request->action) {
            case 'login': $result = (new LoginController)->login($request); break;
            case 'registration': {
                $userRepository = new UserRepository;
                $validation = (new RegistrationController())->userValidation($request, $userRepository);
                if(!empty($validation)) {
                    $result = json_encode(array('status' => 422, 'data' => $validation, 'message' => null));
                } else {
                    $result = (new RegistrationController)->registration($request);
                }
                break;
            }
            case 'modifyProfil': {
                $userRepository = new UserRepository;
                $validation = (new ProfilController())->userValidation($request, $userRepository);
                if(!empty($validation)) {
                    $result = json_encode(array('status' => 422, 'data' => $validation, 'message' => null));
                } else {
                    $result = (new ProfilController)->modifyProfil($request);
                }
                break;
            }
            case 'analysis': $result = (new AnalysisController())->analyze($request); break;
            case 'chat': $result = (new ChatController())->sendMessage($request); break;
            default: $result = json_encode(array('status' => 404, 'data' => array('A keresett oldal nem található!'), 'message' => null));
        }

        echo $result;
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if(isset($_GET['action'])) {

    try {
        $result = '';

        switch($_GET['action']) {
            case 'familyStatus': $result = (new FamilyStatusController)->showFamilyStatuses(); break;
            case 'profilData': $result = (new ProfilController)->showProfilData($_GET['userId']); break;
            case 'messages': $result = (new ChatController)->showmessages($_GET['lastMessageId']); break;
            case 'lastMessageId': $result = (new ChatController)->showLastMessageId(); break;
            default: $result = json_encode(array('status' => 404, 'data' => array('A keresett oldal nem található!'), 'message' => null));
        }

        echo $result;
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}
