<?php
    session_start();

    if(!isset($_SESSION['user'])) {
        header("location: index.php");
    }
?><!DOCTYPE html>
<html lang="HU">
    <head>
        <title>Elemzés</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href="web/index/design.css" rel="stylesheet"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <script src="web/index/jquery-3.7.0.min.js" type="text/javascript"></script>
        <script src="web/index/script.js" type="text/javascript"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </head>

    <body>
        <main class="form w-25 m-auto mt-3">
            <h1 class="h4 mb-3 fw-normal main-title">Bejelentkezve <?php echo $_SESSION['user']['name']; ?></h1>

            <form id="analysisForm">

                <div class="form-floating mb-3 title-div">
                    <h1 class="h4 mb-3 fw-normal floating-title">Elemzés</h1>
                    <a href="loggedIn.php" class="btn btn-primary menu-button padding-button py-2">Vissza</a>
                </div>

                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="url" name="url" placeholder="Link">
                    <label for="url">Link</label>
                </div>

                <input type="hidden" id="action" name="action" value="analysis">

                <button class="btn btn-primary w-100 py-2" type="button" onclick="analysis();">Listázás</button>

                <div class="form-floating mb-3"></div>

                <table id="analysed_data"></table>

                <div class="form-floating danger mb-3 hide">
                    <div class="alert alert-danger" role="alert"></div>
                </div>
            </form>
        </main>
    </body>
</html>