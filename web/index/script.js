let RUNNING = true;
let SERVICE_URL = 'src/Controller/Routing.php';
let DEFAULT_ERROR_MSG = 'Hiba történt a folyamat során!';
let FIRST_TIME_RUNNING = true;

function login() {
    let formData = $('#loginForm').serializeArray().reduce(function(a, x) {a[x.name] = x.value; return a;}, {});
    let validationMessage = loginValidation(formData.email, formData.password);

    if(validationMessage.length > 0) {
        showError(validationMessage.join("<br/>"));
    } else {
        $.ajax({
            type: 'POST',
            cache: true,
            dataType: 'json',
            url: SERVICE_URL,
            data: JSON.stringify(formData),
            success: function (data) {
                if(data.status !== 200) {
                    showError(data.data.join('<br/>'));
                } else {
                    window.location.href = "loggedIn.php";
                }
            },
            error: function () {
                showError(DEFAULT_ERROR_MSG);
            }
        });
    }
}//Bejelentkezés

function loginValidation(email, password) {
    let message = [];

    if(email === '' || password === '') {
        message.push('Kérjük töltse ki mindkét mezőt!');
    }
    if(emailValidation(email) === false) {
        message.push('Érvénytelen e-mail formátumot adott meg!');
    }
    if(passwordValidation(password) === false) {
        message.push('Nem megfelelő jelszót adott meg!<br/>A jelszónak 4 karakterből kell állnia.<br/>Minimum 1 betűt és számot kell, hogy tartalmazzon.');
    }

    return message;
}//Bejelentkezési adatok validálása

function emailValidation(email){
    let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}//E-mail fomrátum validálása

function passwordValidation(password) {
    let regex = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/;
    let passwordLength = password.length;

    if((parseInt(passwordLength) < 4) || regex.test(password) === false) {
        return false;
    } else {
        return true;
    }
}//Jelszó validálás

function showSuccess(message) {
    $('.danger').addClass('hide');
    $('.success').removeClass('hide');
    $('.alert-success').html(message);
}//Sikeres művelet üzenet kiírása

function showError(message) {
    $('.success').addClass('hide');
    $('.danger').removeClass('hide');
    $('.alert-danger').html(message);
}//Hibaüzenet kiírása

function registration() {
    let formData = $('#registrationForm').serializeArray().reduce(function(a, x) {a[x.name] = x.value; return a;}, {});
    let validationMessage = registrationValidation(formData);

    if(validationMessage.length > 0) {
        showError(validationMessage.join("<br/>"));
    } else {
        $.ajax({
            type: 'POST',
            cache: true,
            dataType: 'json',
            url: SERVICE_URL,
            data: JSON.stringify(formData),
            success: function (data) {
                if(data.status !== 201) {
                    showError(data.data.join('<br/>'));
                } else {
                    showSuccess('Sikeres regisztráció!');
                }
            },
            error: function () {
                showError(DEFAULT_ERROR_MSG);
            }
        });
    }
}//Regisztráció

function registrationValidation(formData) {
    let message = [];

    if (formData.name === '' || formData.email === '' || formData.password === '' || formData.password2 === '') {
        message.push('A *-al jelölt mezők kitöltése kötelező!');
    }
    if (parseInt(formData.name.length) < 3) {
        message.push('Nem megfelelő nevet adott meg!<br/>A névnek minimum 4 karakterből kell állnia.');
    }
    if (emailValidation(formData.email) === false) {
        message.push('Érvénytelen e-mail formátumot adott meg!');
    }
    if (passwordValidation(formData.password) === false) {
        message.push('Nem megfelelő jelszót adott meg!<br/>A jelszónak 4 karakterből kell állnia.<br/>Minimum 1 betűt és számot kell, hogy tartalmazzon.');
    }
    if (formData.password !== formData.password2) {
        message.push('A két jelszó nem ugyanaz!');
    }

    return message;
}//Regisztrációs adatok validálása

function getFamilyStatuses() {
    $.ajax({
        type: 'GET',
        cache: true,
        dataType: 'json',
        url: SERVICE_URL+'?action=familyStatus',
        success: function (data) {
            if(data.status !== 200) {
                showError('Hiba történt a családi állapot lekérdezése közben!');
            } else {
                setupFamilyTypeOption(data.data);
            }
        },
        error: function () {
            showError(DEFAULT_ERROR_MSG);
        }
    });
}//Családi állapotok lekérdezése

function setupFamilyTypeOption(data) {
    let id = '';
    let name = '';

    $.each(data, function(key, value) {
        $.each(value, function(k, v) {
            if(k === 'id') {
                id = v;
            } else {
                name = v[0].toUpperCase() + v.slice(1);
            }
        });
        $('#family_type').append(new Option(name, id));
    });
}//Családi állapot adatok betöltése a select option-be

function getProfilData(userId) {
    $.ajax({
        type: 'GET',
        cache: true,
        dataType: 'json',
        url: SERVICE_URL+'?action=profilData&userId='+userId,
        success: function (data) {
            if(data.status !== 200) {
                showError('Hiba történt a felhasználó adatainak lekérdezése közben!');
            } else {
                setupProfilData(data.data);
            }
        },
        error: function () {
            showError(DEFAULT_ERROR_MSG);
        }
    });
}//Felhaszháló adatainak lekérdezése

function setupProfilData(data) {
    $.each(data, function(key, value) {
        if(key === 'family_status_id') {
            value = (value === null) ? 0 : value;
            $("#family_type").val(value).change();
        } else if(key === 'age') {
            $('#'+key).text('Életkor: '+value);
        } else if(key === 'password') {
            $('#'+key).val('');
        } else {
            $('#'+key).val(value);
        }
    });
}//Felhasználó adatainak betöltése az űrlapba

function modifyProfil() {
    let formData = $('#modifyProfilForm').serializeArray().reduce(function(a, x) {a[x.name] = x.value; return a;}, {});
    let validationMessage = profilDataValidation(formData);

    if(validationMessage.length > 0) {
        showError(validationMessage.join("<br/>"));
    } else {
        $.ajax({
            type: 'POST',
            cache: true,
            dataType: 'json',
            url: SERVICE_URL,
            data: JSON.stringify(formData),
            success: function (data) {
                if(data.status !== 200) {
                    showError(data.data.join('<br/>'));
                } else {
                    showSuccess('Sikeres adat módosítás!');
                    setupProfilData(data.data);
                }
            },
            error: function () {
                showError(DEFAULT_ERROR_MSG);
            }
        });
    }
}//Felhasználó adatainak módosítása

function profilDataValidation(formData) {
    let message = [];

    if (formData.name === '' || formData.email === '') {
        message.push('A *-al jelölt mezők kitöltése kötelező!');
    }
    if (parseInt(formData.name.length) < 3) {
        message.push('Nem megfelelő nevet adott meg!<br/>A névnek minimum 4 karakterből kell állnia.');
    }
    if (emailValidation(formData.email) === false) {
        message.push('Érvénytelen e-mail formátumot adott meg!');
    }
    if (formData.password !== '' && passwordValidation(formData.password) === false) {
        message.push('Nem megfelelő jelszót adott meg!<br/>A jelszónak 4 karakterből kell állnia.<br/>Minimum 1 betűt és számot kell, hogy tartalmazzon.');
    }
    if (formData.password !== formData.password2) {
        message.push('A két jelszó nem ugyanaz!');
    }

    return message;
}//Felhasználó adatmódosítási adatok validálása

function analysis() {
    if(RUNNING === false) {
        return;
    }
    RUNNING = false;
    let formData = $('#analysisForm').serializeArray().reduce(function(a, x) {a[x.name] = x.value; return a;}, {});

    if(formData.url === '') {
        showError('A mező kitöltése kötelező!');
        RUNNING = true;
    } else {
        $.ajax({
            type: 'POST',
            cache: true,
            dataType: 'json',
            url: SERVICE_URL,
            data: JSON.stringify(formData),
            success: function (data) {
                if(data.status !== 200) {
                    showError(data.data.join('<br/>'));
                    RUNNING = true;
                } else {
                    if(data.data.length > 0) {
                        createTable(data.data);
                    } else {
                        showError('Nincs adata!');
                    }
                    RUNNING = true;
                }
            },
            error: function () {
                showError(DEFAULT_ERROR_MSG);
                RUNNING = true;
            },
        });
    }
}//Elemzés

function createTable(data) {
    $('#analysed_data').html('');

    $.each(data, function(key, value) {
        if(parseInt(key) === 0) {
            $('#analysed_data').append('<tr>').append('<td>' + value + '</td>');
        } else if(parseInt(key) % 8 === 0) {
            $('#analysed_data').append('</tr>').append('<tr>').append('<td>' + value + '</td>');
        } else {
            $('#analysed_data').append('<td>' + value + '</td>');
        }
    });

    $('#analysed_data').append('</tr>');
    $('#analysed_data td').css('border', '1px solid #000000').css('text-align', 'center').css('padding', '0px 10px');
}//Tábla felépítése az elemzés adataiból

function sendMessage() {
    let formData = $('#chatForm').serializeArray().reduce(function(a, x) {a[x.name] = x.value; return a;}, {});
    if(formData.message.length === 0) {
        showError('Legalább egy karaktert adjon meg üzenetként!');
    } else {
        $.ajax({
            type: 'POST',
            cache: true,
            dataType: 'json',
            url: SERVICE_URL,
            data: JSON.stringify(formData),
            success: function (data) {
                if(data.status !== 201) {
                    showError(data.data.join('<br/>'));
                } else {
                    $('#message').val('');
                }
            },
            error: function () {
                showError(DEFAULT_ERROR_MSG);
            }
        });
    }
}//Chat

function setChatWindowHeight() {
    let windowHeight = $(window).height();
    let maxHeight = parseInt(windowHeight) - 420;
    $('#chatArea').css("max-height", maxHeight);
}//Chat ablak magasság beállítása

function getLastMessageId(userId) {
    $.ajax({
        type: 'GET',
        cache: true,
        dataType: 'json',
        url: SERVICE_URL+'?action=lastMessageId',
        success: function (data) {
            if(data.status !== 200) {
                showError('Hiba történt az utolsó üzenet lekérdezése közben!');
            } else {
                if(FIRST_TIME_RUNNING === true) {
                    document.cookie = 'lastMessageId='+data.data.lastMessageId;
                    getMessages(userId, 0);
                } else {
                    if(parseInt(getCookie('lastMessageId')) !== parseInt(data.data.lastMessageId)) {
                        getMessages(userId, getCookie('lastMessageId'));
                        document.cookie = 'lastMessageId='+data.data.lastMessageId;
                    }
                }
            }
            FIRST_TIME_RUNNING = false;
        },
        error: function () {
            showError(DEFAULT_ERROR_MSG);
        }
    });
}//Utolsó üzenet ID lekérdezés

function getMessages(userId, lastMessageId) {
    $.ajax({
        type: 'GET',
        cache: true,
        dataType: 'json',
        url: SERVICE_URL+'?action=messages&lastMessageId='+lastMessageId,
        success: function (data) {
            if(data.status !== 200) {
                showError('Hiba történt az üzenetek lekérdezése közben!');
            } else {
                showMessages(userId, data.data);
            }
        },
        error: function () {
            showError(DEFAULT_ERROR_MSG);
        }
    });
}//Üzenetek lekérdezése

function showMessages(userId, data) {
    let side = '';
    $('#chatArea').removeClass('hide');

    $.each(data, function(key, value) {
        $.each(value, function(k, v) {
            if(k === 'user_id') {
                if(parseInt(userId) === parseInt(v)) {
                    side = 'right';
                    $('#chatArea').prepend('<div class="message ' + side + '"></div><div class="clear"></div>');
                } else {
                    side = 'left';
                    $('#chatArea').prepend('<div class="message ' + side + '"></div><div class="clear"></div>');
                }
            }
            if(k === 'message') {
                $('#chatArea div').first().append('<p class="messageText">' + v + '</p>');
            }
            if(k === 'created_at') {
                $('#chatArea div').first().append('<span class="createdAt ' + side + '">' + v + '</span>').append('<div style="clear:both"></div>');
            }
            if(k === 'name') {
                $('#chatArea div').first().append('<span class="sendBy ' + side + '">' + v + '</span>');
            }
        });
    });
}//Üzenetek megjelenítése

function getCookie(cookieName) {
    let name = cookieName + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');

    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while(c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if(c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }

    return '';
}