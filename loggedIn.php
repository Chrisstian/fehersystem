<?php
    session_start();

    if(!isset($_SESSION['user'])) {
        header("location: index.php");
    }
?><!DOCTYPE html>
<html lang="HU">
    <head>
        <title>Bejelentkezve</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href="web/index/design.css" rel="stylesheet"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <script src="web/index/jquery-3.7.0.min.js" type="text/javascript"></script>
        <script src="web/index/script.js" type="text/javascript"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </head>

    <body>
        <main class="form w-25 m-auto mt-3">
            <h1 class="h4 mb-3 fw-normal main-title">Bejelentkezve <?php echo $_SESSION['user']['name']; ?></h1>

            <div class="form-floating mb-3">
                <a href="profil.php" class="btn btn-primary floating-button py-2">Profil</a>
                <a href="analysis.php" class="btn btn-primary floating-button py-2">Elemzés</a>
                <a href="chat.php" class="btn btn-primary floating-button py-2">Chat</a>
                <a href="index.php" class="btn btn-primary floating-button py-2">Kijelentkezés</a>
            </div>
        </main>
    </body>
</html>