<?php
    session_start();

    if(!isset($_SESSION['user'])) {
        header("location: index.php");
    }
?><!DOCTYPE html>
<html lang="HU">
    <head>
        <title>Profil</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href="web/index/design.css" rel="stylesheet"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <script src="web/index/jquery-3.7.0.min.js" type="text/javascript"></script>
        <script src="web/index/script.js" type="text/javascript"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
                getFamilyStatuses();
                setTimeout(function() {
                    getProfilData(<?php echo $_SESSION['user']['id']; ?>);
                }, 150);

            });
        </script>
    </head>

    <body>
        <main class="form w-25 m-auto mt-3">
            <h1 class="h4 mb-3 fw-normal main-title">Bejelentkezve <?php echo $_SESSION['user']['name']; ?></h1>

            <form id="modifyProfilForm">

                <div class="form-floating mb-3 title-div">
                    <h1 class="h4 mb-3 fw-normal floating-title">Adatok módosítása</h1>
                    <a href="loggedIn.php" class="btn btn-primary menu-button padding-button py-2">Vissza</a>
                </div>

                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Név" value="">
                    <label for="name">Név *</label>
                </div>

                <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail cím" value="">
                    <label for="email">E-mail cím *</label>
                </div>

                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Jelszó" value="">
                    <label for="password">Jelszó</label>
                </div>

                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="password2" name="password2" placeholder="Jelszó megerősítése" value="">
                    <label for="password">Jelszó megerősítése</label>
                </div>

                <div class="form-floating mb-3">
                    <select class="form-select form-control" name="family_type" id="family_type"></select>
                    <label for="family_type">Családi állapot</label>
                </div>

                <div class="form-floating mb-3">
                    <input type="date" class="form-control" id="birth_date" name="birth_date" placeholder="Születési idő" value="">
                    <label for="birth_date">Születési idő</label>
                </div>

                <div class="form-floating mb-3">
                    <p id="age"></p>
                </div>

                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="web_page" name="web_page" placeholder="Weboldal" value="">
                    <label for="web_page">Weboldal</label>
                </div>

                <input type="hidden" id="action" name="action" value="modifyProfil">
                <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['user']['id']; ?>">

                <button class="btn btn-primary w-100 py-2" type="button" onclick="modifyProfil();">Adatok módosítása</button>

                <div class="form-floating mb-3"></div>

                <div class="form-floating success mb-3 hide">
                    <div class="alert alert-success" role="alert"></div>
                </div>

                <div class="form-floating danger mb-3 hide">
                    <div class="alert alert-danger" role="alert"></div>
                </div>
            </form>
        </main>
    </body>
</html>