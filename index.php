<?php
    session_start();

    if(isset($_SESSION['user'])) {
        session_unset();
        session_destroy();
    }
?><!DOCTYPE html>
<html lang="HU">
    <head>
        <title>Bejelentkezés</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href="web/index/design.css" rel="stylesheet"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <script src="web/index/jquery-3.7.0.min.js" type="text/javascript"></script>
        <script src="web/index/script.js" type="text/javascript"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </head>

    <body>
        <main class="form w-25 m-auto mt-3">
            <form id="loginForm">
                <h1 class="h4 mb-3 fw-normal">Bejelentkezés</h1>

                <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="email" name="email" placeholder="E-mail cím">
                    <label for="email">E-mail cím</label>
                </div>

                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Jelszó">
                    <label for="password">Jelszó</label>
                </div>

                <input type="hidden" id="action" name="action" value="login">

                <button class="btn btn-primary w-100 py-2" type="button" onclick="login();">Bejelentkezés</button>

                <div class="form-floating mb-3"></div>

                <a href="registration.php" class="btn btn-primary w-100 py-2">Regisztráció</a>

                <div class="form-floating mb-3"></div>

                <div class="form-floating danger mb-3 hide">
                    <div class="alert alert-danger" role="alert"></div>
                </div>
            </form>
        </main>
    </body>
</html>